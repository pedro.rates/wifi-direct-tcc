package com.example.pedrorates.wifidirecttest;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pDeviceList;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.net.wifi.p2p.WifiP2pManager.Channel;
import android.net.wifi.p2p.WifiP2pManager.PeerListListener;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by PedroHenrique on 20/04/2018.
 */

public class WiFiDirectBroadcastReceiver extends BroadcastReceiver {

    private WifiP2pManager mManager;
    private Channel mChannel;
    private MainActivity mActivity;
    private PeerListListener mPeerListListener;
    private static final String TAG = "WifiBR";
    public WiFiDirectBroadcastReceiver(WifiP2pManager manager, Channel channel,
                                       MainActivity activity) {
        super();
        this.mManager = manager;
        this.mChannel = channel;
        this.mActivity = activity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();

        if (WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION.equals(action)) {
            Log.d(TAG, "Wifi state changed");
            int state = intent.getIntExtra(WifiP2pManager.EXTRA_WIFI_STATE, -1);
            if (state == WifiP2pManager.WIFI_P2P_STATE_ENABLED) {
                Log.d(TAG,"Wifi p2p enabled");
            } else {
                Log.d(TAG, "Wifi p2p not enabled =(");
            }
            // Check to see if Wi-Fi is enabled and notify appropriate activity
        } else if (WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION.equals(action)) {
            if (mManager != null) {
                Log.d(TAG, "Found wifi devices");
                mManager.requestPeers(mChannel, peerListListener);
            }
        } else if (WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION.equals(action)) {
            Log.d(TAG, "Connection Changed");
            if (mManager == null) {
                Log.d(TAG, "mManager null");
                return;
            }

            NetworkInfo networkInfo = (NetworkInfo) intent
                    .getParcelableExtra(WifiP2pManager.EXTRA_NETWORK_INFO);

            if (networkInfo.isConnected()) {

                // We are connected with the other device, request connection
                // info to find group owner IP
                Log.d(TAG, "Connection successful");
                mManager.requestConnectionInfo(mChannel, connectionInfoListener);
            }
            else {
                Log.d(TAG, "Connection Terminated");
                mActivity.setExitGroupButtonInvisible();
                mActivity.setStatusDefault("Nenhum dispositivo conectado.");
                mActivity.setSelectFileButtonInvisible();
            }
        } else if (WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION.equals(action)) {
            // Respond to this device's wifi state changing
        }


    }

    private List<WifiP2pDevice> peers = new ArrayList<WifiP2pDevice>();

    private WifiP2pManager.PeerListListener peerListListener = new PeerListListener() {
        @Override
        public void onPeersAvailable(WifiP2pDeviceList peerList) {

            Collection<WifiP2pDevice> refreshedPeers = peerList.getDeviceList();
            if (!refreshedPeers.equals(peers)) {
                peers.clear();
                peers.addAll(refreshedPeers);
                Log.d(TAG, peers.size() + " devices found!");

                // If an AdapterView is backed by this data, notify it
                // of the change. For instance, if you have a ListView of
                // available peers, trigger an update.
//                ((WiFiPeerListAdapter) getListAdapter()).notifyDataSetChanged();
                ArrayAdapter<WifiP2pDevice> adapter = mActivity.getWifiDeviceAdapter();
                adapter.clear();
                adapter.addAll(refreshedPeers);
                adapter.notifyDataSetChanged();
                mActivity.hideEmptyListTextView();
                mActivity.showDevicesList();

            }

            if (peers.size() == 0) {
                Log.d(TAG, "No devices found");
                mActivity.showEmptyListTextView();
                return;
            }
        }
    };

    private WifiP2pManager.ConnectionInfoListener connectionInfoListener = new WifiP2pManager.ConnectionInfoListener() {
        @Override
        public void onConnectionInfoAvailable(final WifiP2pInfo info) {

            // InetAddress from WifiP2pInfo struct.
//            InetAddress groupOwnerAddress = InetAddress.getByName(info.groupOwnerAddress.getHostAddress())
            mActivity.setExitGroupButtonVisible();
            mActivity.setWifiP2pInfo(info);
            mActivity.hideDevicesList();

            // After the group negotiation, we can determine the group owner
            // (server).
            if (info.groupFormed && info.isGroupOwner) {
                Log.d(TAG, "This device is the owner");
                mActivity.setStatusSuccess("Conectado como Group Owner");
//                mActivity.startServer();
                mActivity.setSelectFileButtonVisible();
                mActivity.setDownloadFileButtonInvisible();
                // Do whatever tasks are specific to the group owner.
                // One common case is creating a group owner thread and accepting
                // incoming connections.
            } else if (info.groupFormed) {
                Log.d(TAG, "This device is a client");
                mActivity.setStatusSuccess("Conectado como Cliente");
                mActivity.sendAck();

                // The other device acts as the peer (client). In this case,
                // you'll want to create a peer thread that connects
                // to the group owner.
            }
        }
    };

}
