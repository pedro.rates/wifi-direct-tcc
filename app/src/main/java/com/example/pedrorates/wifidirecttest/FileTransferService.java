package com.example.pedrorates.wifidirecttest;

import android.Manifest;
import android.app.IntentService;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.net.TrafficStats;
import android.net.Uri;
import android.os.Environment;
import android.os.SystemClock;
import android.support.annotation.MainThread;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

public class FileTransferService extends IntentService {

    private static final int SOCKET_TIMEOUT = 5000;
    public static final String ACTION_SEND_FILE = "com.example.pedrorates.wifidirecttest.SEND_FILE";
    public static final String EXTRAS_FILE_PATH = "file_url";
    public static final String EXTRAS_GROUP_OWNER_ADDRESS = "go_host";
    public static final String EXTRAS_GROUP_OWNER_PORT = "go_port";
    public static final String ACTION_DOWNLOAD_FILE = "com.example.pedrorates.wifidirecttest.DOWNLOAD_FILE";
    public static final String ACTION_SEND_ACK = "com.example.pedrorates.wifidirecttest.ACK";
    public static final String ACTION_SEND_NOTIFICATION = "com.example.pedrorates.wifidirecttest.SEND_NOTIFICATION";
    public static final String ACTION_DOWNLOAD_FILES = "com.example.pedrorates.wifidirecttest.DOWNLOAD_FILES";
    public static final String DOWNLOAD_QUEUE = "com.example.pedrorates.wifidirecttest.DOWNLOADQUEUE";

    public FileTransferService(String name) {
        super(name);
    }
    public FileTransferService() {
        super("FileTransferService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {

        Context context = getApplicationContext();

        if (intent.getAction().equals(ACTION_SEND_FILE)) {

            String fileUri = intent.getExtras().getString(EXTRAS_FILE_PATH);
            String host = intent.getExtras().getString(EXTRAS_GROUP_OWNER_ADDRESS);
            Socket socket = new Socket();
            int port = intent.getExtras().getInt(EXTRAS_GROUP_OWNER_PORT);
            try {
                Log.d(MainActivity.TAG, "Opening client socket - ");
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), SOCKET_TIMEOUT);
                Log.d(MainActivity.TAG, "Client socket - " + socket.isConnected());
                OutputStream stream = socket.getOutputStream();
                ContentResolver cr = context.getContentResolver();
                InputStream is = null;
                try {
                    is = cr.openInputStream(Uri.parse(fileUri));
                } catch (FileNotFoundException e) {
                    Log.d(MainActivity.TAG, e.toString());
                }
                Util.copyFile(is, stream, true);
                Log.d(MainActivity.TAG, "Client: Data written");
            } catch (IOException e) {
                Log.e(MainActivity.TAG, e.getMessage());
            } finally {
                if (socket != null) {
                    if (socket.isConnected()) {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            // Give up
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (intent.getAction().equals(ACTION_DOWNLOAD_FILE)) {
            String host = intent.getExtras().getString(EXTRAS_GROUP_OWNER_ADDRESS);
            Socket socket = new Socket();
            int port = intent.getExtras().getInt(EXTRAS_GROUP_OWNER_PORT);
            try {

                Log.d(MainActivity.TAG, "Opening client socket - ");
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), SOCKET_TIMEOUT);
                Log.d(MainActivity.TAG, "Client socket - " + socket.isConnected());

                InputStream is = null;

                OutputStream outputStream = socket.getOutputStream();

                String request = "DownloadRequest";

                InputStream inputStream = new ByteArrayInputStream(request.getBytes(Charset.forName("UTF-8")));
                Util.copyFile(inputStream, outputStream, false);

                Log.d(MainActivity.TAG, "Client: File requested");

            } catch (IOException e) {
                Log.e(MainActivity.TAG, e.getMessage());
            } finally {
                if (socket != null) {
                    if (socket.isConnected()) {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            // Give up
                            e.printStackTrace();
                        }
                    }
                }
            }

        } else if (intent.getAction().equals(ACTION_SEND_ACK)) {
            String host = intent.getExtras().getString(EXTRAS_GROUP_OWNER_ADDRESS);
            Socket socket = new Socket();
            int port = intent.getExtras().getInt(EXTRAS_GROUP_OWNER_PORT);
            try {

                Log.d(MainActivity.TAG, "Opening client socket - ");
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), SOCKET_TIMEOUT);
                Log.d(MainActivity.TAG, "Client socket - " + socket.isConnected());

                OutputStream outputStream = socket.getOutputStream();

                String ack = "ack";

                InputStream inputStream = new ByteArrayInputStream(ack.getBytes(Charset.forName("UTF-8")));
                Util.copyFile(inputStream, outputStream, true);

                Log.d(MainActivity.TAG, "ACK sent");

            } catch (IOException e) {
                Log.e(MainActivity.TAG, e.getMessage());
            } finally {
                if (socket != null) {
                    if (socket.isConnected()) {
                        try {
                            socket.close();
                        } catch (IOException e) {
                            // Give up
                            e.printStackTrace();
                        }
                    }
                }
            }
        } else if (intent.getAction().equals(ACTION_SEND_NOTIFICATION)) {
            for (String clientSocket : MainActivity.clients) {
                Socket socket = new Socket();
                String host = clientSocket;
                int port = 8443;
                try {
//                    if (socket.isConnected()) {
                    Log.d(MainActivity.TAG, "Opening client socket - ");
                    socket.bind(null);
                    socket.connect((new InetSocketAddress(host, port)), SOCKET_TIMEOUT);
                    Log.d(MainActivity.TAG, "Client socket - " + socket.isConnected());
//                    }

                    OutputStream outputStream = socket.getOutputStream();

                    String ack = "Not:file ready";

                    InputStream inputStream = new ByteArrayInputStream(ack.getBytes(Charset.forName("UTF-8")));
                    Util.copyFile(inputStream, outputStream, true);

                    Log.d(MainActivity.TAG, "Not sent");

                } catch (IOException e) {
                    Log.e(MainActivity.TAG, e.getMessage());
                } finally {
                    if (socket != null) {
                        if (socket.isConnected()) {
                            try {
                                socket.close();
                            } catch (IOException e) {
                                // Give up
                                e.printStackTrace();
                            }
                        }
                    }
                }
            }
        } else if (intent.getAction().equals(ACTION_DOWNLOAD_FILES)) {
            int[] downloadQueue = intent.getIntArrayExtra(DOWNLOAD_QUEUE);
            for (int i = 0; i < downloadQueue.length; i++) {
                downloadFile(intent, downloadQueue[i]);
            }

        }
    }

    public void downloadFile(Intent intent, int fileNum) {
        String host = intent.getExtras().getString(EXTRAS_GROUP_OWNER_ADDRESS);
        Socket socket = new Socket();
        int port = intent.getExtras().getInt(EXTRAS_GROUP_OWNER_PORT);
        try {

            Log.d(MainActivity.TAG, "Opening client socket - ");
            socket.bind(null);
            socket.connect((new InetSocketAddress(host, port)), SOCKET_TIMEOUT);
            Log.d(MainActivity.TAG, "Client socket - " + socket.isConnected());


            String request = "Dow";

            OutputStream outputStream = socket.getOutputStream();
            if (fileNum > 9) {
                request = request + "2" + fileNum;
            } else {
                request = request + "1" + fileNum;
            }

            InputStream inputStream = new ByteArrayInputStream(request.getBytes(Charset.forName("UTF-8")));
            Util.copyFile(inputStream, outputStream, false);

            Log.d(MainActivity.TAG, "Client: File requested");

        } catch (IOException e) {
            Log.e(MainActivity.TAG, e.getMessage());
        } finally {
            if (socket != null) {
                if (socket.isConnected()) {
                    try {
                        socket.close();
                    } catch (IOException e) {
                        // Give up
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public void startImageHandler(String result) {
        if (result != null) {
            Intent intent = new Intent();
            intent.setAction(android.content.Intent.ACTION_VIEW);
            intent.setDataAndType(Uri.parse("file://" + result), "image/*");
            this.startActivity(intent);
        }
    }

}
