package com.example.pedrorates.wifidirecttest;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class Util {

    private static String TAG = "Util";

    public static boolean copyFile(InputStream inputStream, OutputStream out, boolean canClose) {
        byte buf[] = new byte[1024];
        int len;
        try {
            while ((len = inputStream.read(buf)) != -1) {
                out.write(buf, 0, len);
            }
            out.close();
            inputStream.close();
        } catch (IOException e) {
            Log.d(TAG, e.toString());
            return false;
        }
        return true;
    }

    public static boolean copyFileWithPreAppend(InputStream inputStream, OutputStream out, String preAppendString) {
        byte buf[] = new byte[1024];
        int len;
        byte[] preAppend;
        preAppend = preAppendString.getBytes(Charset.forName("UTF-8"));
        try {
            out.write(preAppend);
            while ((len = inputStream.read(buf)) != -1) {
                out.write(buf, 0, len);
            }
            out.close();
            inputStream.close();
        } catch (IOException e) {
            Log.d(TAG, e.toString());
            return false;
        }
        return true;
    }

    public static String convertStreamToString(java.io.InputStream is) {
        java.util.Scanner s = new java.util.Scanner(is).useDelimiter("\\A");
        return s.hasNext() ? s.next() : "";
    }

    public static String DecodeFileNumber(InputStream is) {
        try {
            byte[] isBytes = new byte[1];
            is.read(isBytes, 0, 1);
            String message = new String(isBytes, Charset.forName("UTF-8"));
            if (message.equals("1")) {
                isBytes = new byte[1];
                is.read(isBytes, 0, 1);
            } else {
                isBytes = new byte[2];
                is.read(isBytes, 0, 2);
            }
            message = new String(isBytes, Charset.forName("UTF-8"));
            return message;
        } catch (Exception e) {
            e.printStackTrace();
            return "1";
        }
    }

    public static String getFormattedDateTime() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss");
        Date now = Calendar.getInstance().getTime();
        String nowString = simpleDateFormat.format(now).toString();
        return nowString;
    }

}
