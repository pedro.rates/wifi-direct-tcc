package com.example.pedrorates.wifidirecttest;

import android.net.wifi.p2p.WifiP2pDevice;

public class CustomDevice extends WifiP2pDevice {

    @Override
    public String toString() {
        StringBuffer sbuf = new StringBuffer();
        sbuf.append("Device: ").append(deviceName);
        sbuf.append("\n deviceAddress: ").append(deviceAddress);
        return sbuf.toString();
    }
}
