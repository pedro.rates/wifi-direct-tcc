package com.example.pedrorates.wifidirecttest;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.IntentService;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.Uri;
import android.net.wifi.WpsInfo;
import android.net.wifi.p2p.WifiP2pConfig;
import android.net.wifi.p2p.WifiP2pDevice;
import android.net.wifi.p2p.WifiP2pInfo;
import android.net.wifi.p2p.WifiP2pManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PushbackInputStream;
import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "com.wifidirect.dccfutebol.MESSAGE";
    WifiP2pManager mManager;
    WifiP2pManager.Channel mChannel;
    BroadcastReceiver mReceiver;
    IntentFilter mIntentFilter;
    ListView wifiDevicesListView;
    ArrayAdapter<WifiP2pDevice> wifiDevicesAdapter;
    List<WifiP2pDevice> devicesList;
    TextView emptyListTextView;
    public static final String TAG = "MainActivity";
    Menu menu;
    MenuItem refreshItem;
    MenuItem stopRefreshItem;
    TextView statusTextView;
    Button exitGroupButton;
    Button selectFileButton;
    private WifiP2pInfo info;
    Button downloadFileButton;
    public boolean serverStarted = false;
    TextView selectedFileTextView;
    Button setSelectedFileNullButton;
    public static final int USER_INITIATED = 0x1000;
    public static ArrayList<String> clients = new ArrayList<String>();
    Button sendNotificationButton;
    public static int goIntent = 7;
    Button setGoIntentButton;
    public static MainActivity mainActivity;
    Button test1Button;
    public static int[] availabilityArray = new int[50];
    public static int[] downloadArray = new int[50];
    FileServerAsync fileServerAsync;
    Button test2Button;
    public static ProgressDialog progressBar;
    public static int fileDownloadCounter = 0;
    Button test3Button;
    String logAtivo = "";
    Double[] downloadTimeArray = new Double[50];





    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);

        mainActivity = MainActivity.this;

        mManager = (WifiP2pManager) getSystemService(Context.WIFI_P2P_SERVICE);
        mChannel = mManager.initialize(this, getMainLooper(), null);
        mReceiver = new WiFiDirectBroadcastReceiver(mManager, mChannel, this);


        wifiDevicesListView = findViewById(R.id.devices_list);
        wifiDevicesAdapter = new ArrayAdapter<WifiP2pDevice>(this,
                android.R.layout.simple_list_item_1);

        wifiDevicesListView.setAdapter(wifiDevicesAdapter);

        wifiDevicesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                WifiP2pDevice deviceClicked = wifiDevicesAdapter.getItem(position);
                connect(deviceClicked);
                Log.d("ClickListener", deviceClicked.deviceName);
            }
        });

        emptyListTextView = (TextView) findViewById(R.id.empty_list_text_view);
        emptyListTextView.setText("Nenhum dispositivo encontrado.");

        statusTextView = (TextView) findViewById(R.id.connection_status_text_view);
        statusTextView.setText("Nenhum dispositivo conectado");
        statusTextView.setTextColor(getResources().getColor(R.color.disabledColor));

        exitGroupButton = findViewById(R.id.exit_group_button);
        exitGroupButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                removeGroup();
            }
        });
        setExitGroupButtonInvisible();

//        selectFileButton = findViewById(R.id.select_file_button);
//        selectFileButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                intent.setType("*/*");
//                startActivityForResult(intent, 20);
//
//            }
//        });

//        downloadFileButton = findViewById(R.id.download_file_button);
//        downloadFileButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                requestFile();
//            }
//        });

//        setSelectedFileNullButton = findViewById(R.id.set_selected_file_null_button);
//        setSelectedFileNullButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                setSelectedFileNull();
//            }
//        });

//        selectedFileTextView = (TextView) findViewById(R.id.selected_file_text_view);
//        setSelectedFileNull();

        sendNotificationButton = findViewById(R.id.send_notification_button);
        sendNotificationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendNotification();
            }
        });

        setGoIntentButton = findViewById(R.id.set_go_intent_button);
        setGoIntentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showNumberPicker();
            }
        });

        test1Button = findViewById(R.id.test1_button);
        test1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logAtivo = "teste1";
                fileServerAsync.internetDownloadLog("Started download at: " +  Calendar.getInstance().getTime());
                showDownloadProgress();
                if (fileServerAsync != null) {
                    sendNotification();
                    fileServerAsync.downloadFilesFromWeb();
                }
            }
        });

        test2Button = findViewById(R.id.test2_button);
        test2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logAtivo = "teste2";
                fileServerAsync.internetDownloadLog("Started download at: " +  Calendar.getInstance().getTime());
                showDownloadProgress();
                requestMultipleFiles();
            }
        });

        test3Button = findViewById(R.id.test3_button);
        test3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                logAtivo = "teste3";
                fileServerAsync.internetDownloadLog("Started download at: " +  Calendar.getInstance().getTime());
                showDownloadProgress();
                if (fileServerAsync != null) {
                    fileServerAsync.downloadFilesFromWeb();
                }
            }
        });


        mIntentFilter = new IntentFilter();
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_STATE_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_PEERS_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_CONNECTION_CHANGED_ACTION);
        mIntentFilter.addAction(WifiP2pManager.WIFI_P2P_THIS_DEVICE_CHANGED_ACTION);

        initializeAvArray();

        startServer();
    }


    public void showNumberPicker() {

        final Dialog d = new Dialog(MainActivity.this);
        d.setTitle("NumberPicker");
        d.setContentView(R.layout.number_picker_dialog);
        Button b1 = d.findViewById(R.id.confirm_go_intent_button);
        final NumberPicker np = d.findViewById(R.id.numberPicker1);
        np.setMaxValue(14);
        np.setMinValue(0);
        np.setWrapSelectorWheel(false);
        b1.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                goIntent = np.getValue();
                d.dismiss();
            }
        });
        d.show();
    }

    public ArrayAdapter<WifiP2pDevice> getWifiDeviceAdapter() {
        return  wifiDevicesAdapter;
    }

    public ListView getDevicesList() {
        return  wifiDevicesListView;
    }

    public void showDevicesList() {
        wifiDevicesListView.setVisibility(View.VISIBLE);
    }

    public void hideDevicesList() {
        wifiDevicesListView.setVisibility(View.GONE);
    }

    public TextView getEmptyListTextView() {
        return  emptyListTextView;
    }

    public void hideEmptyListTextView () {
        emptyListTextView.setVisibility(View.GONE);
    }

    public void showEmptyListTextView () {
        emptyListTextView.setVisibility(View.VISIBLE);
    }


    public void setStatusSuccess(String status) {
        if (statusTextView != null) {
            statusTextView.setText(status);
            statusTextView.setTextColor(getResources().getColor(R.color.successColor));
        }
    }

    public void setStatusWarning(String status) {
        if (statusTextView != null) {
            statusTextView.setText(status);
            statusTextView.setTextColor(getResources().getColor(R.color.warningColor));
        }
    }

    public void setStatusError(String status) {
        if (statusTextView != null) {
            statusTextView.setText(status);
            statusTextView.setTextColor(getResources().getColor(R.color.errorColor));
        }
    }

    public void setStatusDefault(String status) {
        if (statusTextView != null) {
            statusTextView.setText(status);
            statusTextView.setTextColor(getResources().getColor(R.color.defaultColor));
        }
    }


    public void setExitGroupButtonVisible() {
        if (exitGroupButton != null) {
            exitGroupButton.setVisibility(View.VISIBLE);
        }

    }

    public void setExitGroupButtonInvisible() {
        if (exitGroupButton != null) {
            exitGroupButton.setVisibility(View.GONE);
        }
    }

    public void setSelectFileButtonVisible() {
        if (selectFileButton != null) {
            selectFileButton.setVisibility(View.VISIBLE);
        }

    }

    public void setSelectFileButtonInvisible() {
        if (selectFileButton != null) {
            selectFileButton.setVisibility(View.GONE);
        }
    }

    public void setDownloadFileButtonVisible() {
        if (downloadFileButton != null) {
            downloadFileButton.setVisibility(View.VISIBLE);
        }
    }

    public void setDownloadFileButtonInvisible() {
        if (downloadFileButton != null) {
            downloadFileButton.setVisibility(View.GONE);
        }
    }

    public void setWifiP2pInfo(WifiP2pInfo info) {
        this.info = info;
    }

    public void initializeAvArray() {
        ArrayList<Integer> downloadColleciton = new ArrayList<Integer>();
        for (int i = 0; i < availabilityArray.length; i++) {
            availabilityArray[i] = 0;
            downloadColleciton.add(i + 1);
        }
        Collections.shuffle(downloadColleciton);
        shuffleArray(downloadColleciton);
    }

    static void shuffleArray(ArrayList<Integer> downloadColleciton)
    {
        for (int i = 0; i < downloadColleciton.size(); i++) {
            downloadArray[i] = downloadColleciton.get(i);
        }
    }


    @Override
    protected  void onResume() {
        super.onResume();
        registerReceiver(mReceiver, mIntentFilter);
    }

    @Override
    protected  void onPause() {
        super.onPause();
        unregisterReceiver(mReceiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        FileServerAsync.closeServerSocket();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        refreshItem = (MenuItem) menu.findItem(R.id.action_refresh);
        stopRefreshItem = (MenuItem) menu.findItem(R.id.action_stop_refresh);
        stopRefreshItem.setVisible(false);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                // User chose the "Settings" item, show the app settings UI...
                this.searchForPeers();
                return true;
            case R.id.action_stop_refresh:
                // User chose the "Settings" item, show the app settings UI...
                this.stopPeerSearch();
                return true;
            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);

        }
    }

    public void searchForPeers() {
        mManager.discoverPeers(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Started peer discovery successfully!");
                statusTextView.setText("Procurando dispositivos...");
                statusTextView.setTextColor(getResources().getColor(R.color.warningColor));
                refreshItem.setVisible(false);
                stopRefreshItem.setVisible(true);
            }


            @Override
            public void onFailure(int reasonCode) {
                Log.d(TAG, "Failed at starting peer discovery!");
            }
        });
    }

    public void stopPeerSearch() {
        mManager.stopPeerDiscovery(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                refreshItem.setVisible(true);
                stopRefreshItem.setVisible(false);
                Log.d(TAG, "Stopped peer discovery successfully!");
            }

            @Override
            public void onFailure(int reason) {
                Log.d(TAG, "Failed at stopping peer discovery!");
            }
        });
    }

    public void connect(WifiP2pDevice device) {

        WifiP2pConfig config = new WifiP2pConfig();
        config.deviceAddress = device.deviceAddress;
        config.wps.setup = WpsInfo.PBC;
        config.groupOwnerIntent = MainActivity.goIntent;

        mManager.connect(mChannel, config, new WifiP2pManager.ActionListener() {

            @Override
            public void onSuccess() {
                statusTextView.setText("Iniciando a conexão.");
                statusTextView.setTextColor(getResources().getColor(R.color.successColor));
                // WiFiDirectBroadcastReceiver notifies us. Ignore for now.
            }

            @Override
            public void onFailure(int reason) {
                Toast.makeText(MainActivity.this, "A conexão falhou. Tente novamente.",
                        Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void removeGroup() {
        FileServerAsync.closeServerSocket();
        serverStarted = false;
        mManager.removeGroup(mChannel, new WifiP2pManager.ActionListener() {
            @Override
            public void onSuccess() {
                Log.d(TAG, "Successfully removed group");
            }

            @Override
            public void onFailure(int reason) {
                Log.d(TAG, "Error removing group");
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User has picked an image. Transfer it to group owner i.e peer using
        // FileTransferService.
        if (data == null) {
            return;
        }
        Uri uri = data.getData();
        Log.d(TAG, "Intent----------- " + uri);
        FileServerAsync.fileUri = uri.toString();
        selectedFileTextView.setText("Selected file: " + uri.toString());

    }

    public void sendAck() {
        Intent serviceIntent = new Intent(MainActivity.this, FileTransferService.class);
        serviceIntent.setAction(FileTransferService.ACTION_SEND_ACK);
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                info.groupOwnerAddress.getHostAddress());
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_PORT, 8443);
        this.startService(serviceIntent);
    }

    public void requestFile() {
        Intent serviceIntent = new Intent(MainActivity.this, FileTransferService.class);
        serviceIntent.setAction(FileTransferService.ACTION_DOWNLOAD_FILE);
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                info.groupOwnerAddress.getHostAddress());
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_PORT, 8443);
        this.startService(serviceIntent);
    }

    public void requestMultipleFiles() {
        View v = findViewById(android.R.id.content);
        showDownloadProgress();
        Intent serviceIntent = new Intent(MainActivity.this, FileTransferService.class);
        serviceIntent.setAction(FileTransferService.ACTION_DOWNLOAD_FILES);
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_ADDRESS,
                info.groupOwnerAddress.getHostAddress());
        serviceIntent.putExtra(FileTransferService.EXTRAS_GROUP_OWNER_PORT, 8443);
        serviceIntent.putExtra(FileTransferService.DOWNLOAD_QUEUE, downloadArray);
        this.startService(serviceIntent);
    }

    public void sendNotification() {
        Intent serviceIntent = new Intent(MainActivity.this, FileTransferService.class);
        serviceIntent.setAction(FileTransferService.ACTION_SEND_NOTIFICATION);
        this.startService(serviceIntent);
    }

    public static void displayToast(Context mContext, String text) {
        Toast.makeText(mContext, text, Toast.LENGTH_SHORT).show();
    }


    public void startServer () {
        if (!serverStarted) {
            fileServerAsync = new FileServerAsync(this);
            fileServerAsync.execute();
            serverStarted = true;
        }
    }

    public void setSelectedFileNull() {
        FileServerAsync.fileUri = null;
        selectedFileTextView.setText("Selected file: none");
    }

    public static void showDownloadProgress(){
        fileDownloadCounter = 0;
        mainActivity.runOnUiThread(new Runnable() {
            public void run() {
                progressBar = new ProgressDialog(mainActivity);
                progressBar.setMessage("Baixando arquivos");
                progressBar.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                progressBar.setIndeterminate(false);
                progressBar.setProgress(0);
                progressBar.setMax(50);
                progressBar.show();
            }
        });

    }

    public static void updateDownloadProgress() {
        progressBar.setProgress(fileDownloadCounter);
        /*if (fileDownloadCounter >= 50) {
            progressBar.setMessage("Download finalizado!");
            progressBar.dismiss();
        }*/
    }

    public static class FileServerAsync extends AsyncTask<Void, Void, String> {

        private Context context;

        public static String fileUri;

        public FileServerAsync(Context context) {
            this.context = context;
            fileUri = "file://" + Environment.getExternalStorageDirectory() + "/"
                    + context.getPackageName() + "/files/arq";
        }

        public static boolean cancellationToken = false;

        @Override
        protected String doInBackground(Void... params) {
            try {

                /**
                 * Create a server socket and wait for client connections. This
                 * call blocks until a connection is accepted from a client
                 */
                ServerSocket serverSocket = new ServerSocket(8443);

                while(!cancellationToken) {
                    Socket client = serverSocket.accept();
                    InputStream cis = client.getInputStream();
                    byte[] isBytes = new byte[3];
                    cis.read(isBytes, 0, 3);
                    String message = new String(isBytes, Charset.forName("UTF-8"));

                    switch (message) {
                        case "ack":
                            addClientToList(client);
                            break;
                        case "Dow":
                            message = Util.DecodeFileNumber(cis);
                            sendFileToClient(client, message);
                            break;
                        case "Not":
                            mainActivity.logAtivo = "teste1";
                            internetDownloadLog("Started download at: " +  Calendar.getInstance().getTime());
                            mainActivity.requestMultipleFiles();
                            break;
                        case "404":
                            message = Util.DecodeFileNumber(cis);
                            downloadSingleFileFromWeb(Integer.parseInt(message), true);
                            break;
                        default:
                            message = Util.DecodeFileNumber(cis);
                            receiveFile(cis, message);
                            break;
                    }
                }
                return "";
            } catch (IOException e) {
                Log.e(MainActivity.TAG, e.getMessage());
                return null;
            }
        }

        /**
         * Start activity that can handle the JPEG image
         */
        @Override
        protected void onPostExecute(String result) {
            if (result != null) {
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse("file://" + result), "image/*");
                context.startActivity(intent);
            }
        }

        public static void closeServerSocket() {
            cancellationToken = true;
        }

        public void addClientToList(Socket client) {
            if (!clients.contains(client.getInetAddress().getHostAddress())) {
                clients.add(client.getInetAddress().getHostAddress());
            }
        }

        private void sendFileToClient(Socket client, String fileNum) {
            String host = client.getInetAddress().getHostAddress();
            Socket socket = new Socket();
            int port = 8443;
            try {
                Log.d(MainActivity.TAG, "Opening client socket - ");
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), 5000);
                Log.d(MainActivity.TAG, "Client socket - " + socket.isConnected());
                OutputStream stream = socket.getOutputStream();
                ContentResolver cr = context.getContentResolver();
                InputStream is;
                String currentFile = fileUri + fileNum + ".bin";
                if (Integer.parseInt(fileNum) > 9) {
                    fileNum = "2" + fileNum;
                } else {
                    fileNum = "1" + fileNum;
                }
                try {
                    is = cr.openInputStream(Uri.parse(currentFile));
                    Util.copyFileWithPreAppend(is, stream, "img" + fileNum);
                } catch (FileNotFoundException e) {
                    Log.d(MainActivity.TAG, e.toString());
                    String fileNotAvailableString = "404" + fileNum;
                    is = new ByteArrayInputStream(fileNotAvailableString.getBytes(Charset.forName("UTF-8")));
                    Util.copyFileWithPreAppend(is, stream, fileNotAvailableString);
                }
                Log.d(MainActivity.TAG, "Client: Data written");
            } catch (IOException e) {
                Log.e(MainActivity.TAG, e.getMessage());
            }
        }

        private void receiveFile(InputStream is, String fileNumber) {
            try {
                mainActivity.fileDownloadCounter += 1;
                mainActivity.updateDownloadProgress();
                long initialTime = SystemClock.elapsedRealtime();

                File f = new File(Environment.getExternalStorageDirectory() + "/"
                        + context.getPackageName() + "/files/arq" + fileNumber + ".bin");

                String fileNum = Util.DecodeFileNumber(is);
                File dirs = new File(f.getParent());
                if (!dirs.exists())
                    dirs.mkdirs();
                f.createNewFile();
                Util.copyFile(is, new FileOutputStream(f), true);
                long finalTime = SystemClock.elapsedRealtime();
                float elapsedTime = finalTime - initialTime;
                elapsedTime /= 1000;
                float fileSize = f.length() / (1024.0f * 1024.0f);
                float avgSpeed = fileSize / elapsedTime;
                String logInfo = "Received file: " + fileNum + "\r\n";
                logInfo += "Time to download: " + elapsedTime + " segundos\r\n" ;
                logInfo += "File size: " + fileSize + " MB\r\n";
                logInfo += "Average speed: " + avgSpeed + " MB/s\r\n";
                Log.d(MainActivity.TAG, "Time to download: " + elapsedTime + " segundos");
                Log.d(MainActivity.TAG, "File size: " + fileSize + " MB");
                Log.d(MainActivity.TAG, "Average speed: " + avgSpeed + " MB/s");
                appendLog(logInfo);
                logInfo = Util.getFormattedDateTime() + "," + elapsedTime + "," + fileSize;
                internetDownloadLog(logInfo);

                Log.d(MainActivity.TAG, "real size: " + f.length());
            }
            catch (IOException e) {
                Log.e(MainActivity.TAG, e.getMessage());
            }
        }

        private void askForFile(Socket client, InputStream inputStream, String fileNum) {
            String host = client.getInetAddress().getHostAddress();
            Socket socket = new Socket();
            int port = 8443;
            try {
                Log.d(MainActivity.TAG, "Opening client socket - ");
                socket.bind(null);
                socket.connect((new InetSocketAddress(host, port)), 5000);
                Log.d(MainActivity.TAG, "Client socket - " + socket.isConnected());


                OutputStream outputStream = socket.getOutputStream();

//                String request = "DownloadRequest";

                String request = "Dow" + fileNum;
                InputStream is = new ByteArrayInputStream(request.getBytes(Charset.forName("UTF-8")));
                Util.copyFile(is, outputStream, false);
            } catch (IOException e) {
                Log.e(MainActivity.TAG, e.getMessage());
            }
        }

        void openReceivedFile(String result) {
            if (result != null) {
                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                intent.setDataAndType(Uri.parse("file://" + result), "image/*");
                context.startActivity(intent);
            }
        }

        public void downloadFilesFromWeb() {
            for(int i = 1; i < 51; i++) {
                downloadSingleFileFromWeb(i, false);
            }
        }

        public void downloadSingleFileFromWeb(final int fileNum, final boolean isFallback) {
            RequestQueue queue = Volley.newRequestQueue(mainActivity.getApplicationContext());
            String url = "http://vmserver.winet.dcc.ufmg.br/FILES/500k/arq" + fileNum;

            final double tempoInicial = System.currentTimeMillis();
            mainActivity.downloadTimeArray[fileNum - 1] = tempoInicial;
            // Request a string response from the provided URL.
            StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            double tempoInicialArquivo = mainActivity.downloadTimeArray[fileNum - 1];
                            double tempoTotal = System.currentTimeMillis() - tempoInicialArquivo;
                            double fileSize = saveDownloadedFile(response, fileNum);
                            availabilityArray[fileNum - 1] = 1;
                            mainActivity.fileDownloadCounter += 1;
                            updateDownloadProgress();
                            String logInfo = Util.getFormattedDateTime() + "," + tempoTotal + "," + fileSize;
                            internetDownloadLog(logInfo);
                            if (isFallback) {
                                String logMessage = "File " + fileNum + " not found in GO. Downloaded from 4g";
                                appendLog(logMessage);
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d(MainActivity.TAG, "Error: " + error
                    );
                }
            });

            // Add the request to the RequestQueue.
            stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                    10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
            queue.add(stringRequest);
        }

        public double saveDownloadedFile(String file, int fileNumber) {
            final File downloadedFile = new File(Environment.getExternalStorageDirectory() + "/"
                    + context.getPackageName() + "/files/arq" + fileNumber + ".bin");
            try
            {
                File dirs = new File(downloadedFile.getParent());
                if (!dirs.exists())
                    dirs.mkdirs();
                downloadedFile.createNewFile();
                InputStream is = new ByteArrayInputStream(file.getBytes(Charset.forName("UTF-8")));
                Util.copyFile(is, new FileOutputStream(downloadedFile), true);
                Log.d(TAG, "File " + fileNumber + " downloaded!");
                double fileSize = downloadedFile.length() / (1024.0f * 1024.0f);
                return fileSize;
            }
            catch (IOException e)
            {
                e.printStackTrace();
                return 0;
            }
        }

        public void appendLog(String logInfo)
        {

            final File logFile = new File(Environment.getExternalStorageDirectory() + "/"
                    + context.getPackageName() + "/logs/downloadLog.txt");

            String logHeader = "Download Log: " + Calendar.getInstance().getTime() + "\r\n";
            logHeader += logInfo;

            if (!logFile.exists())
            {
                try
                {
                    File dirs = new File(logFile.getParent());
                    if (!dirs.exists())
                        dirs.mkdirs();
                    logFile.createNewFile();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            try
            {
                //BufferedWriter for performance, true to set append to file flag
                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
                buf.append(logHeader);
                buf.newLine();
                buf.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        public void internetDownloadLog(String logInfo)
        {

            final File logFile = new File(Environment.getExternalStorageDirectory() + "/"
                    + context.getPackageName() + "/logs/" + mainActivity.logAtivo + ".csv");

            if (!logFile.exists())
            {
                try
                {
                    File dirs = new File(logFile.getParent());
                    if (!dirs.exists())
                        dirs.mkdirs();
                    logFile.createNewFile();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            try
            {
                //BufferedWriter for performance, true to set append to file flag
                BufferedWriter buf = new BufferedWriter(new FileWriter(logFile, true));
                buf.append(logInfo);
                buf.newLine();
                buf.close();
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }
    }

}
