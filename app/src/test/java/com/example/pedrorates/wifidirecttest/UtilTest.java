package com.example.pedrorates.wifidirecttest;

import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.nio.charset.Charset;

import static junit.framework.Assert.assertEquals;

public class UtilTest {

    @Test
    public void ShouldReturnTwoDigitNumber() {
        String testString = "210";
        InputStream is = new ByteArrayInputStream(testString.getBytes(Charset.forName("UTF-8")));
        String answer = Util.DecodeFileNumber(is);
        assertEquals(answer, "10");
    }

    @Test
    public void ShouldReturnOneDigitNumber() {
        String testString = "15";
        InputStream is = new ByteArrayInputStream(testString.getBytes(Charset.forName("UTF-8")));
        String answer = Util.DecodeFileNumber(is);
        assertEquals(answer, "5");
    }
}
